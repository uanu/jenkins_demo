import java.io.*;
import java.util.StringTokenizer;

public class MyProcess {

    public static void main(String[] argv) throws IOException {
        FileReader fr = new FileReader("car.csv");
        BufferedReader br = new BufferedReader(fr);
        String s = "";
        PrintStream out = new PrintStream("salemean.txt");
        s = br.readLine();
        while ((s = br.readLine()) != null) {
            int sum = 0;
            StringTokenizer st = new StringTokenizer(s, ",");
            out.print(st.nextToken() + ",");
            for (int i = 0; i < 2; i++)
                st.nextToken();
            for (int i = 0; i < 5; i++)
                sum += Integer.parseInt(st.nextToken());
            out.println(1.0 * sum / 5);
        }

    }
}
