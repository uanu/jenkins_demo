import java.io.*;

public class FileIoDemo {
    public static void main(String[] av) {
        try {
            // Method 1:
            FileIO.copyFile("FileIO.java", "FileIO.bak1");
            // Method 2:
            BufferedInputStream is = new BufferedInputStream(new FileInputStream("FileIO.java"));
            BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream("FileIO.bak2"));
            FileIO.copyFile(is, os, true);
            // Method 3:
            FileReader fr = new FileReader("FIleIO.java");
            FileWriter fw = new FileWriter("FileIO.bak3");
            FileIO.copyFile(fr, fw, true);
            // Method 4:
            PrintWriter pw = new PrintWriter("FileIO.bak4");
            FileIO.copyFile("FileIO.java", pw, true);
            // Method 5:
            File fi = new File("FileIO.java");
            File fo = new File("FileIO.bak5");
            FileIO.copyFile(fi, fo);
        } catch (IOException e) {
            System.err.println(e);
        }
    }
}