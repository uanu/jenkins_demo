import java.io.*;

public class MyData implements Serializable {
    public static void main(String[] argv) throws IOException, ClassNotFoundException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String name = br.readLine();
        String clear = br.readLine();
        MyData mydata = new MyData(name, clear);
        System.out.println("保存前: " + mydata.toString());
        // 持久化
        String fileName = "saved.dat";
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName));
        oos.writeObject(mydata);
        oos.close();
        // 修改
        mydata.setUserName("xxx");
        System.out.println("修改后: " + mydata.toString());
        // 恢复
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName));
        MyData pre = (MyData) ois.readObject();
        System.out.println("保存的: " + pre.toString());
    }

    private static final long serialVersionUID = -4965296908339881739L;
    String userName;
    String passwordCypher;
    transient String passwordClear;

    /**
     * This constructor is required by most APIs
     */
    public MyData() {
        // Nothing to do
    }

    public MyData(String name, String clear) {
        setUserName(name);
        setPassword(clear);
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String s) {
        this.userName = s;
    }

    public String getPasswordCypher() {
        return passwordCypher;
    }

    /**
     * Save the clear text p/w in the object, it won't get serialized
     * So we must save the encryption! Encryption not shown here.
     */
    static String encrypt(String s) {
        return s;
    }

    public void setPassword(String s) {
        this.passwordClear = s;
        passwordCypher = encrypt(passwordClear);
    }

    public String toString() {
        return "MyData[" + userName + ",------]";
    }
}