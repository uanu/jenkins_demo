import java.io.Console;
import java.io.IOException;

public class ConsoleRead {
    public static void main(String[] args) {
        try {
            Console is = System.console();
            if (is == null)
                throw new RuntimeException("console error");
            String name = System.console().readLine("What is your name?");
            System.out.println("Hello, " + name.toUpperCase());
        } catch (RuntimeException e) {
            System.out.println("Console open error: " + e);
        }

    }
}