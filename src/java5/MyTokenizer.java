import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MyTokenizer {
    public static Pattern p = Pattern.compile("([A-Za-z0-9]+[.-_])*[A-Za-z0-9]+@[A-Za-z0-9-]+(\\.[A-Z|a-z]{2,})+");

    public static void main(String[] argv) throws IOException {
        String path = "./demo2.txt";
        FileReader fr = new FileReader(path);
        BufferedReader br = new BufferedReader(fr);
        StringBuilder all = new StringBuilder();
        String s = "";
        while ((s = br.readLine()) != null) {
            all.append(s + "\n");
        }
        Matcher m = p.matcher(all);
        if (!m.find())
            System.out.println("不存在邮箱");
        else {
            System.out.println("存在邮箱");
            do {
                System.out.println(m.group());
                StringTokenizer st = new StringTokenizer(m.group(), "@");
                System.out.println("用户名: " + st.nextToken() + "邮箱域名: " + st.nextToken());
            } while (m.find());
        }

    }
}

